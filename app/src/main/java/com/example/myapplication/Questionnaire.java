package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import java.util.Iterator;
import java.util.Vector;

public class Questionnaire extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questionnaire);

    }

    public Vector<QuestionReponse> quest;

    public Questionnaire() {
        quest = new Vector<QuestionReponse>();
        quest.add(new QuestionReponse("Combien de pas faites-vous chaque jour?", 10000));
        quest.add(new QuestionReponse("Combien de fois pratiquez-vous une activité sportive chaque semaine?", 2));
        quest.add(new QuestionReponse("Combien de portions de fruit et légumes mangez-vous chaque jour en moyenne?", 5));

    }

    public Iterator<QuestionReponse> getQRIterator() {
        return quest.iterator();
    }



}
