package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class QuestionReponse extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_reponse);
    }

    private final String question;
    private final int reponse;

    public QuestionReponse(String question, int reponse){
        this.question = question;
        this.reponse = reponse;
    }

    public String getQuestion(){
        return question;
    }

    public int getReponse(){
        return reponse;
    }

}