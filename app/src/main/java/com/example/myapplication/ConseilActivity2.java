package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ConseilActivity2 extends AppCompatActivity {
int imc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conseil2);
        // recuperer avec getExtra les données poids et taille envoyer depuis MainActivity


        TextView IMC = findViewById(R.id.affichageIMC);

        if (imc<18){
            IMC.setText("Votre IMC est de "+ imc+"\nVous êtes en sous poids");
        } else if(imc>30){
            IMC.setText("Votre IMC est de "+ imc+"\nVous êtes obèse, pas encore morbide");

        }else{
            IMC.setText("Votre IMC est de "+ imc+"ok");
        }
    }


    public void passerAuQuestionnaire(View view) {
        Intent passerAuQuest = new Intent();
        passerAuQuest.setClass(getApplicationContext(),Questionnaire.class);
        startActivity(passerAuQuest);
    }
}


